## APP版本管理

>`本模块需要授权登录，在HTTP请求头HEADER中传入参数ACCESS-TOKEN  CLIENT-VERSION(3位数字)`


#### 101、检查版本-游戏场景json

调用地址
>POST > [域名]/api/saas/checkVersionOfScene

>POST > [域名]/api/saas/v6/checkVersionOfScene [v6]

>POST > [域名]/api/saas/v61/checkVersionOfScene [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|

返回值：
```
{
    "code": 0,//返回码 0成功，1失败
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "SceneList": [
                {
                    "SceneID": 46,
                    "SceneDescrip": "Test3",
                    "LevelName": "Test3",
                    "ThumbImage": "Test3.png",
                    "SceneVideo": "Test3.png",
                    "ScenePak": "Test3.pak",
                    "DownLoaded": true,
                    "ContentLength": 5.6,
                    "Transform": "123456",
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 159
        },
        "Private": {//私有版本 若无最新版本则为空对象
            "SceneList": [
                {
                    "SceneID": 46,
                    "SceneDescrip": "Test3",
                    "LevelName": "Test3",
                    "ThumbImage": "Test3.png",
                    "SceneVideo": "Test3.png",
                    "ScenePak": "Test3.pak",
                    "DownLoaded": true,
                    "ContentLength": 5.6,
                    "Transform": "1234567",
                    "Version": 1,
                    "SupportClientVersion": []
                }
            ],
            "VersionCode": 160
        }
    },
    "msg": "OK"
}
```

#### 102、检查版本-虚拟屏json

调用地址
>POST > [域名]/api/saas/checkVersionOfScreen

>POST > [域名]/api/saas/v6/checkVersionOfScreen [v6]

>POST > [域名]/api/saas/v61/checkVersionOfScreen [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|


返回值：
```
{
    "code": 0,
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 62,
                    "Name": "XuNiPing_19",
                    "Descrip": "XuNiPing_19",
                    "ThumbImage": "XuNiPing_19.png",
                    "Pak": "XuNiPing_19.pak",
                    "PakSize": "0.2",
                    "DownLoaded": false,
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 122
        },
        "Private": {}//私有版本 若无最新版本则为空对象
    },
    "msg": "OK"
}
```

#### 103、检查版本-组件json

调用地址
>POST > [域名]/api/saas/checkVersionOfModule

>POST > [域名]/api/saas/v6/checkVersionOfModule [v6]

>POST > [域名]/api/saas/v61/checkVersionOfModule [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|

返回值：
```
{
    "code": 0,
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 6,
                    "Name": "DengLong_01",
                    "Descrip": "DengLong_01",
                    "ThumbImage": "DengLong_01.png",
                    "Pak": "DengLong_01.pak",
                    "PakSize": "2.0",
                    "DownLoaded": false,
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 122
        },
        "Private": {}//私有版本 若无最新版本则为空对象
    },
    "msg": "OK"
}
```

#### 104、检查版本-特效json

调用地址
>POST > [域名]/api/saas/checkVersionOfEffect

>POST > [域名]/api/saas/v6/checkVersionOfEffect [v6]

>POST > [域名]/api/saas/v61/checkVersionOfEffect [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|

返回值：
```
{
    "code": 0,
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 7,
                    "Name": "VFX_02",
                    "Descrip": "VFX_02",
                    "ThumbImage": "VFX_02.png",
                    "Pak": "VFX_02.pak",
                    "PakSize": "23.4",
                    "DownLoaded": false
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 116
        },
        "Private": {}//私有版本 若无最新版本则为空对象
    },
    "msg": "OK"
}
```

#### 105、检查版本-全景图json

调用地址
>POST > [域名]/api/saas/checkVersionOfPanorama

>POST > [域名]/api/saas/v6/checkVersionOfPanorama [v6]

>POST > [域名]/api/saas/v61/checkVersionOfPanorama [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|

返回值：
```
{
    "code": 0,
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 17,
                    "Name": "thatch_chapel_4k",
                    "Descrip": "教堂",
                    "ThumbImage": "thatch_chapel_4k.png",
                    "Pak": "thatch_chapel_4k.hdr",
                    "PakSize": "25.8",
                    "DownLoaded": false,
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 108
        },
        "Private": {}//私有版本 若无最新版本则为空对象
    },
    "msg": "OK"
}
```

#### 106、检查版本-虚拟人son

调用地址
>POST > [域名]/api/saas/v6/checkVersionOfVirturePerson [v6]

>POST > [域名]/api/saas/v61/checkVersionOfVirturePerson [v61]

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|


返回值：
```
{
    "code": 0,
    "data": {
        "Public": {//共有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 64,
                    "Name": "Test3",
                    "Descrip": "Test31",
                    "ThumbImage": "Test3.png",
                    "Pak": "Test3.pak",
                    "PakSize": "5.6",
                    "DownLoaded": false,
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 106
        },
        "Private": {//私有版本 若无最新版本则为空对象
            "List": [
                {
                    "Id": 64,
                    "Name": "Test3",
                    "Descrip": "Test31",
                    "ThumbImage": "Test3.png",
                    "Pak": "Test3.pak",
                    "PakSize": "5.6",
                    "DownLoaded": false,
                    "Version": 2,
                    "SupportClientVersion": ["610","620"]
                }
            ],
            "VersionCode": 105
        }
    },
    "msg": "OK"
}
```