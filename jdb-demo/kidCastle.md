## 抠像-相册上传整合成1个

#### 101、相册上传

调用地址
>POST > [域名]/api/kidCastle/uploadPhotoZip

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| class_id         | string          | 必选            |  15299                               | 班级ID                    |
| student_branch   | string          | 必选            |  2018112900007                       | 学生编号                    |
| student_cnname   | string          | 必选            |  张婷婷                              | 学生中文名                    |
| student_enname   | string          | 必选            |  Ting                                | 学生英文名                    |
| album_cnname     | string          | 必选            |  WK2升级相册六单元                   | 相册模板中文名                    |
| album_enname     | string          | 必选            |  WK2_6                               | 相册模板英文名                    |
| photo_type       | string          | 必选            |  1                                   | 相册类型：1图片相册 2视频相册     |
| current_course_level_cn  | string  | 必选            |  幼儿初阶                            | 相册级别中文名                    |
| current_course_level_en  | string  | 必选            |  youerchujie                         | 相册级别英文名                    |
| zip                      | form binary  | 必选       |  xxx                                 | 上传压缩包文件,格式zip 表单方式上传|

返回值：
```
{
    "errorcode": 0,
    "msg": "ok",
    "data": {
        "photo_zip_url": "http://jdb-demo.4ukids.cn:9000/kidCastle/wxShareZip/index.html?zip=aHR0cDovL2pkYi1kZW1vLm9zcy1jbi16aGFuZ2ppYWtvdS5hbGl5dW5jcy5jb20va2lkQ2FzdGxlL3Bob3RvV3hTaGFyZVppcC8xMTExLzIwMTkwNjIxMDAxMDJfRWFzb24uemlw",
        "photo_h5_url": "http://oss-jdb-demo.4ukids.cn/kidCastle/html/photoWxShare/index.html?class_id=1111&photo_name=20200926000151_liuyonghu"
    }
}
```
