## 配音API-H5

#### 101、视频列表

调用地址
>POST > [域名]/api/index

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| ACCESS-TOKEN     | string          | 必选            |  0F242BEB-33A4-7CFB-BA85-950EC16F8485    | 访问令牌，放入header头部,后期开发预留|
| user_level       | string          | 可选            |  3                                       | 用户等级                   |

返回值：
```
{
    "errorcode": "0",//请求成功
    "msg": "查询成功",
    "data": {
		"recomment":{
			"video_id" :"123"
			"video_img":"http://xxx.png",//这里图片尺寸最大边600PX
		},
		"list":[
			{
				"video_id"     :"456",
				"video_img"    :"http://xxx.png",//这里图片尺寸最大边300PX
				"video_name_cn":"视频中文名",
				"video_name_en":"视频英文名"
			},
			{
				"video_id"     :"456",
				"video_img"    :"http://xxx.png",//这里图片尺寸最大边300PX
				"video_name_cn":"视频中文名",
				"video_name_en":"视频英文名"
			},
			{
				"video_id"     :"456",
				"video_img"    :"http://xxx.png",//这里图片尺寸最大边300PX
				"video_name_cn":"视频中文名",
				"video_name_en":"视频英文名"
			}
		]
    }
}
```

#### 102、视频详情

调用地址
>POST > [域名]/api/video/detail

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| ACCESS-TOKEN     | string          | 必选            |  0F242BEB-33A4-7CFB-BA85-950EC16F8485    | 访问令牌，放入header头部,后期开发预留                   |
| video_id         | string          | 必选            |  3                                       | 视频ID                   |

返回值：
```
{
    "errorcode": "0",//请求成功
    "msg": "查询成功",
    "data": {
		"video_url"     : "http://xxx.mp4",
		"video_name_cn" : "他是个警察",
		"video_rank"    : "3",//难易级别 1-10
		"video_desc"    : "视频简介",
		"video_duration": "150",//视频总时长(秒)
		"video_no_sound": "http://xxx.mp4" //无原声版视频
    }
}
```

#### 103、获取视频配音信息

调用地址
>POST > [域名]/api/video/dubbing

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| ACCESS-TOKEN     | string          | 必选            |  0F242BEB-33A4-7CFB-BA85-950EC16F8485    | 访问令牌，放入header头部,后期开发预留                   |
| video_id         | string          | 必选            |  3                                       | 视频ID                   |

返回值：
```
{
    "errorcode": "0",//请求成功
    "msg": "查询成功",
    "data": [
		{
			"section_no"          :"1",//第几个配音片段 编号从1正序排列
			"section_duration"    :"3",//配音片段时长
			"section_start"       :"10",//配音片段从第几秒开始
			"section_text_en"     :"Oh,no!", //配音片段英文内容
			"section_text_cn"     :"噢，不!", //配音片段中文内容
			"section_total"       :"7", //配音总计条数
			"section_orign_sound" :"http://xxx.mp3" //配音原声
		},
		{
			"section_no"          :"1",//第几个配音片段 编号从1正序排列
			"section_duration"    :"3",//配音片段时长
			"section_start"       :"10",//配音片段从第几秒开始
			"section_text_en"     :"Oh,no!", //配音片段英文内容
			"section_text_cn"     :"噢，不!", //配音片段中文内容
			"section_total"       :"7", //配音总计条数
			"section_orign_sound" :"http://xxx.mp3" //配音原声
		},
		{
			"section_no"          :"1",//第几个配音片段 编号从1正序排列
			"section_duration"    :"3",//配音片段时长
			"section_start"       :"10",//配音片段从第几秒开始
			"section_text_en"     :"Oh,no!", //配音片段英文内容
			"section_text_cn"     :"噢，不!", //配音片段中文内容
			"section_total"       :"7", //配音总计条数
			"section_orign_sound" :"http://xxx.mp3" //配音原声
		}
	]
}
```

#### 104、上传配音文件

调用地址
>POST > [域名]/api/uploadFileByH5

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| ACCESS-TOKEN     | string          | 必选            |  0F242BEB-33A4-7CFB-BA85-950EC16F8485    | 访问令牌，放入header头部,后期开发预留                   |
| video_id         | string          | 必选            |  1                                       | 当前视频ID |
| file             | Binary          | 必选            |  xxx                                     | 配音文件文件,二进制上传，说明：每次请求只能传1个section的配音文件，文件格式为mp3|

返回值：
```
{
    "errorcode": "0",//请求成功
    "msg": "ok",
    "data": {
		"remain_section_no": [],//剩余未上传的scction
    }
}
```

#### 105、视频合成

调用地址
>POST > [域名]/api/mixVideoByH5

参数

| 参数名称         | 参数类型        |  必填与否       | 样例取值                             | 参数说明                          |
|:----------------:|:---------------:|:---------------:|:------------------------------------:|:---------------------------------:|
| ACCESS-TOKEN     | string          | 必选            |  0F242BEB-33A4-7CFB-BA85-950EC16F8485    | 访问令牌，放入header头部,后期开发预留                   |
| video_id         | string          | 必选            |  1                                       | 当前视频ID |

返回值：
```
{
    "errorcode": "0",//请求成功
    "msg": "ok",
    "data": {
		"mix_video_url": "http://xxx.mp4"//合成后的视频地址
    }
}
```